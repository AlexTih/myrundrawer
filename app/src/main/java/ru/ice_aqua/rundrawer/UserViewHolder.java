package ru.ice_aqua.rundrawer;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import de.hdodenhof.circleimageview.CircleImageView;


class UserViewHolder extends RecyclerView.ViewHolder {

    Button up;
    ImageButton del, share;
    TextView tvName, tvDate, tvDistance, tvTime, tvSpeed;
    CircleImageView imageView;

    public UserViewHolder(View itemView) {
        super(itemView);
        del = (ImageButton) itemView.findViewById(R.id.btn_del);
        share = (ImageButton) itemView.findViewById(R.id.btn_share);
        up = (Button) itemView.findViewById(R.id.btn_up);
        tvName = (TextView) itemView.findViewById(R.id.card_tv_name);
        tvDate = (TextView) itemView.findViewById(R.id.card_tv_date);
        tvDistance = (TextView) itemView.findViewById(R.id.card_tv_distance);
        tvTime = (TextView) itemView.findViewById(R.id.card_tv_time);
        tvSpeed = (TextView) itemView.findViewById(R.id.card_tv_speed);
        imageView = (CircleImageView) itemView.findViewById(R.id.photo_in_rv);
    }
}
