package ru.ice_aqua.rundrawer.models;

public class UserModel {
    private String date;
    private String distance;
    private String id;
    private String name;
    private double speed;
    private String time;



    public UserModel(){}

    public UserModel(String date, String distance, String id, String name, double speed, String time){

        this.date = date;
        this.distance = distance;
        this.id = id;
        this.name = name;
        this.speed = speed;
        this.time = time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getDate() {
        return date;
    }

    public String getDistance() {
        return distance;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getSpeed() {
        return speed;
    }

    public String getTime() {
        return time;
    }

}
